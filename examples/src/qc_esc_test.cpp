/****************************************************************************
 * Copyright (c) 2017 The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name The Linux Foundation nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * NO EXPRESS OR IMPLIED LICENSES TO ANY PARTY'S PATENT RIGHTS ARE GRANTED BY 
 * THIS LICENSE.
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * In addition Supplemental Terms apply.  See the SUPPLEMENTAL file.
 *
 ****************************************************************************/

/*
 * This example shows how to use the UART communication to perform the following tests:
 *  - detect all connected ESCs
 *  - request version information from all ESCs
 *  - send a fixed open-loop command to all ESCs and cycle LED colors (LEDs connected to ESCs)
 *  - send a fixed closed-loop RPM command to all ESCs and cycle LED colors
 */

//TODO: support 6 and 8 props

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "qc_esc_packet.h"
#include "qc_esc_packet_types.h"

//platform.c will contain the platform-specific functions for UARt communication
#include "platform.c"

using namespace std;

#define VERSION "1.0"

#define MAX_NUM_PROPS 8

//uncomment to print bytes of the feedback packets
//#define PRINT_FEEDBACK_PACKET_BYTES

uint32_t print_feedback = 1;

EscPacket fb_packet;

uint8_t rx_buf[128];
uint8_t tx_buf[128];

uint32_t num_detected = 0;
uint32_t num_fb_packets[MAX_NUM_PROPS]  = { 0, 0, 0, 0, 0, 0, 0, 0};
int32_t  esc_sw_versions[MAX_NUM_PROPS] = {-1,-1,-1,-1,-1,-1,-1,-1};
int32_t  esc_hw_versions[MAX_NUM_PROPS] = {-1,-1,-1,-1,-1,-1,-1,-1};
uint32_t esc_unique_ids[MAX_NUM_PROPS]  = { 0, 0, 0, 0, 0, 0, 0, 0};


void parse_esc_feedback_packet(EscPacket * fb)
{
  uint8_t packet_type = qc_esc_packet_get_type(fb);
  uint8_t packet_size = qc_esc_packet_get_size(fb);
  uint8_t * data      = qc_esc_packet_get_data_ptr(fb);
  
  if ( (packet_type==ESC_PACKET_TYPE_FB_RESPONSE) && ((packet_size==11) || (packet_size==12) || (packet_size==16)) )
  {
    uint8_t id         = data[0] >> 4;          //id of the ESC providing feedback
    uint8_t state      = data[0] & 0b00001111;  //state of the ESC
    uint16_t rpm;
    memcpy(&rpm,&(data[1]),sizeof(uint16_t));   //current RPM
    uint8_t cntr       = data[3];               //number of control packets received by the ESC
    int8_t pwr         = data[4];               //current power applied (-100 to 100). Negative means braking
    int8_t raw_voltage = data[5];               //measured voltage value (needs to be scaled)
    
    uint16_t voltage_raw     = 0;               //1mV resolution
    uint16_t current_raw     = 0;               //8mA resolution
    int16_t temperature_raw  = 0;               //deg C * 100
  
    float voltage     = 0;                      //Volts
    float current     = 0;                      //Aamps
    float temperature = 0;                      //deg C
  
    if (packet_size==11)                        //old packet format
      voltage = raw_voltage / 34.0 + 9.0;

    else if (packet_size>=12)                   //new format (higher resolution)
    {
      memcpy(&voltage_raw,&data[5],2);
      voltage = voltage_raw * 0.001;            //convert to Volts
    }
    
    if (packet_size==16)
    {
      memcpy(&current_raw,&data[7],2);
      memcpy(&temperature_raw,&data[9],2);
      temperature = temperature_raw * 0.01;     //convert to degrees C
      current     = current_raw     * 0.008;    //convert to Amps (raw is in 8mA resolution)
    }
 
    if (print_feedback)
    {
      debug_print("ESC response: id %2d, state %2d, cntr %3d, pwr %+04d, %5dRPM, %2.3fV, %2.3fA, %2.3fC \r\n",
                                                      id,state,cntr,pwr,rpm,voltage,current,temperature);
    }
    
    if (id <MAX_NUM_PROPS)
      num_fb_packets[id]++;
  }
}

void parse_esc_version_packet(EscPacket * fb)
{
  QC_ESC_VERSION_INFO info;
  memcpy(&info,fb->buffer,sizeof(info));
  if (info.id < MAX_NUM_PROPS)
  {
    uint8_t esc_id = info.id;
    esc_sw_versions[esc_id] = info.sw_version;
    esc_hw_versions[esc_id] = info.hw_version;
    esc_unique_ids[esc_id]  = info.unique_id;

    //debug_print("ESC: got version info packet! %d %d %d %d\n",esc_id, info.sw_version, info.hw_version, info.unique_id);
  }
  else
  {
    debug_print("ESC: got version info packet from bad id : %d\n",info.id);
  }
}

//read feedback from the ESC
void receive_esc_feedback()
{
  uint32_t timeout_us = 1000;
  int nchars = com_port_read(rx_buf,sizeof(rx_buf),timeout_us);
  
  //if (nchars > 0) debug_print("got %d chars\r\n",nchars);
  
  for (int ii=0; ii<nchars; ii++)
  {
    int16_t packet_length = qc_esc_packet_process_char(rx_buf[ii],&fb_packet);
    
    if (packet_length>0)
    {
      uint8_t   ptype = qc_esc_packet_get_type(&fb_packet);
      //uint8_t * data  = qc_esc_packet_get_data_ptr(&fb_packet);
      //uint8_t   psize = qc_esc_packet_get_size(&fb_packet);
      
      //debug_print("got packet of type %d, length %d!\r\n",ptype,packet_length);
#ifdef PRINT_FEEDBACK_PACKET_BYTES
      debug_print("HEX: ");
      for (int ii=0; ii<packet_length; ii++)
        debug_print("0x%02X ",fb_packet.buffer[ii]);
      
      debug_print("\r\nDEC: ");
      for (int ii=0; ii<packet_length; ii++)
        debug_print("%d ",fb_packet.buffer[ii]);
      debug_print("\r\n");
#endif

      if (ptype == ESC_PACKET_TYPE_FB_RESPONSE)      parse_esc_feedback_packet(&fb_packet);
      if (ptype == ESC_PACKET_TYPE_VERSION_RESPONSE) parse_esc_version_packet(&fb_packet);
    }
  }
}

int send_rpms(int16_t * rpms)
{
  static uint32_t cntr = 0;
  cntr++;
  
  int32_t packet_length = qc_esc_create_rpm_packet4_fb(rpms[0],rpms[1],rpms[2],rpms[3],0,0,0,0,cntr,tx_buf,sizeof(tx_buf));
    
  if (packet_length < 1) { debug_print("could not pack packet: err %d\n",packet_length); return -1; }
    
  //send the packet to serial port
  int ret2 = com_port_write(tx_buf,packet_length);
  if (ret2 != packet_length) { debug_print("could not send data: err %d\n",ret2); return -2; }
  
  return 0;
}


int detect_esc_response(uint32_t number_of_escs)
{
  int16_t rpms[MAX_NUM_PROPS] = {0,0,0,0,0,0,0,0};
  uint32_t ntry = 15;
  
  if ((number_of_escs<4) || (number_of_escs>8))
  {
    debug_print("bad number of escs : %d",number_of_escs);
    return -1;
  }

  debug_print("Detecting ESC response (sending zero RPM commands and requesting feedback)..\n");
  
  //send out zero rpm commands (while automatically requesting feedback) to see if ESCs are alive
  for (uint32_t ii=0; ii<(number_of_escs*ntry); ii++)
  {
    send_rpms(rpms);
    sleep_us(2000);        //wait for ESC to respond
    receive_esc_feedback();
  }

  for (uint32_t ii=0; ii<number_of_escs; ii++)
  {
    if (num_fb_packets[ii] > 0)
      num_detected++;
  }

  debug_print("got esc feedback (out of %d packets each): %d %d %d %d %d %d %d %d\n",
             ntry,
             num_fb_packets[0],num_fb_packets[1],num_fb_packets[2],num_fb_packets[3],
             num_fb_packets[4],num_fb_packets[5],num_fb_packets[6],num_fb_packets[7]);

  debug_print("Found %d ESCs\r\n\n",num_detected);
  
  if (num_detected == number_of_escs)  return 0;
  else                                 return -1;
}


int send_version_request_by_id(uint8_t id)
{
  int32_t packet_length = qc_esc_create_version_request_packet(id, tx_buf,sizeof(tx_buf));

  if (packet_length < 1)
  {
    debug_print("Could not wrap ESC version request packet\n");
    return -1;
  }

  //send the packet to serial port
  int ret2 = com_port_write(tx_buf,packet_length);
  if (ret2 != packet_length) { debug_print("could not send data: err %d\n",ret2); return -2; }

  return 0;
}


int detect_esc_versions(int number_of_escs)
{
  if ((number_of_escs<4) || (number_of_escs>8))
  {
    debug_print("Bad number of ESCs : %d",number_of_escs);
    return -1;
  }

  
  debug_print("Detecting ESC versions..\n");
  
  for (int ii=0; ii<number_of_escs; ii++)
  {
    const int ntry = 5;
    for (int jj=0; jj<ntry; jj++)
    {
      //debug_print("trying esc %d version query (attempt %d)\n",ii,jj);
      send_version_request_by_id(ii);

      sleep_us(2000);  //wait for ESC to respond
      receive_esc_feedback();
      
      if (esc_sw_versions[ii] >= 0)
        break;
    }
  }

  debug_print("Got ESC sw versions : %d %d %d %d %d %d %d %d\n",
             esc_sw_versions[0],esc_sw_versions[1],esc_sw_versions[2],esc_sw_versions[3],
             esc_sw_versions[4],esc_sw_versions[5],esc_sw_versions[6],esc_sw_versions[7]);

  debug_print("Got ESC hw versions : %d %d %d %d %d %d %d %d\n",
             esc_hw_versions[0],esc_hw_versions[1],esc_hw_versions[2],esc_hw_versions[3],
             esc_hw_versions[4],esc_hw_versions[5],esc_hw_versions[6],esc_hw_versions[7]);

  //debug_print("got esc unique ids : %d %d %d %d %d %d %d %d\n",
  //           esc_unique_ids[0],esc_unique_ids[1],esc_unique_ids[2],esc_unique_ids[3],
  //           esc_unique_ids[4],esc_unique_ids[5],esc_unique_ids[6],esc_unique_ids[7]);

  debug_print("*** -1 means no response from that ESC (no ESC or version query is not supported)\n\n");
  
  return 0;
}


int open_loop_test(uint32_t power)
{
  int32_t cntr = 0;
  
  if (power > 100)
  {
    error_print("bad input to open loop test : %d\n",power);
    return -1;
  }
  
  while(1)
  {
    sleep_us(10000);         //wait for ESC to respond
    receive_esc_feedback();
    
    int16_t pwm = power*8;  //between 0 and 800
    
    cntr++;
    if (cntr<20) { pwm = 0; } //send zero RPM to reset possible stall condition
   
    //set the LED color
    uint8_t led = 0b00000000;      //off
    
    if      (cntr % 400 < 100)  led |= QC_ESC_LED_RED_ON;    //red
    else if (cntr % 400 < 200)  led |= QC_ESC_LED_GREEN_ON;  //green
    else if (cntr % 400 < 300)  led |= QC_ESC_LED_BLUE_ON;   //blue
    else
    {
      led |= QC_ESC_LED_RED_ON;    //red
      led |= QC_ESC_LED_GREEN_ON;  //green
      led |= QC_ESC_LED_BLUE_ON;   //blue
    }
    
    //create the motor control packet (PWM mode (open-loop) or RPM mode (closed-loop)
    int32_t packet_length = qc_esc_create_pwm_packet4_fb(pwm,pwm,pwm,pwm,led,led,led,led,cntr,tx_buf,sizeof(tx_buf));
    
    if (packet_length < 1) { debug_print("could not pack packet: err %d\n",packet_length); return -1; }
    
    //send the packet to serial port
    int ret2 = com_port_write(tx_buf,packet_length);
    if (ret2 != packet_length) { debug_print("could not send data: err %d\n",ret2); return -2; }
  }
  
  return 0;
}

int closed_loop_test(uint32_t rpm_des)
{
  int32_t cntr = 0;
  
  while(1)
  {
    sleep_us(10000);        //wait for ESC to respond
    receive_esc_feedback();
    
    int16_t rpm = rpm_des;
    
    cntr++;
    if (cntr<20) { rpm = 0;} //send zero RPM to reset possible stall condition
   
    //set the LED color
    uint8_t led = 0b00000000;      //off
    
    //cycle led colors
    if      (cntr % 400 < 100)  led |= QC_ESC_LED_RED_ON;    //red
    else if (cntr % 400 < 200)  led |= QC_ESC_LED_GREEN_ON;  //green
    else if (cntr % 400 < 300)  led |= QC_ESC_LED_BLUE_ON;   //blue
    else
    {
      led |= QC_ESC_LED_RED_ON;    //red
      led |= QC_ESC_LED_GREEN_ON;  //green
      led |= QC_ESC_LED_BLUE_ON;   //blue
    }
    
    //create the motor control packet (PWM mode (open-loop) or RPM mode (closed-loop)
    int32_t packet_length = qc_esc_create_rpm_packet4_fb(rpm,rpm,rpm,rpm,led,led,led,led,cntr,tx_buf,sizeof(tx_buf));
    
    if (packet_length < 1) { debug_print("could not pack packet: err %d\n",packet_length); return -1; }
    
    //send the packet to serial port
    int ret2 = com_port_write(tx_buf,packet_length);
    if (ret2 != packet_length) { debug_print("could not send data: err %d\n",ret2); return -2; }
  }
  
  return 0;
}

int main(int argc, char * argv[])
{
  debug_print("\r\nqc_esc_test version %s build date %s\n",VERSION,__DATE__);
  debug_print("Usage: qc_esc_test <test_mode> <device path> <baud_rate>\n");
  debug_print("Available test modes:\n");
  debug_print("\t 0: detect esc response\n");
  debug_print("\t 1: detect esc versions\n");
  debug_print("\t 2: open-loop test\n");
  debug_print("\t 3: closed-loop RPM test\n");
  
  char * dev    = (char*)"/dev/ttyUSB0";
  int baud_rate = 250000;
  int test_type = 0;

  if (argc > 1) test_type = atoi(argv[1]);
  if (argc > 2) dev       = argv[2];
  if (argc > 3) baud_rate = atoi(argv[3]);

  debug_print("Trying device %s, baud rate %d\n",dev,baud_rate);
  
  if (com_port_connect(dev,baud_rate))
  {
    debug_print("could not connect to device\n");
    return -1;
  }
  
  
  debug_print("Starting Test #%d\r\n",test_type);
  
  uint16_t desired_pwr = 30;     //out of 100
  uint16_t desired_rpm = 4000;   //RPM
  
  switch(test_type)
  {
    case 0:  detect_esc_response(4);             break;
    case 1:  detect_esc_versions(4);             break;
    case 2:  open_loop_test(desired_pwr);        break;
    case 3:  closed_loop_test(desired_rpm);      break;
    default: debug_print("unknown test type\n"); break;
  }
  
  com_port_disconnect();
  return 0;
}
