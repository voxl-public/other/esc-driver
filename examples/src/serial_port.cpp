/****************************************************************************
 * Copyright (c) 2017 The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name The Linux Foundation nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * NO EXPRESS OR IMPLIED LICENSES TO ANY PARTY'S PATENT RIGHTS ARE GRANTED BY 
 * THIS LICENSE.
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * In addition Supplemental Terms apply.  See the SUPPLEMENTAL file.
 *
 ****************************************************************************/

#include "serial_port.hpp"
#include <stdlib.h>
#include <stdio.h>
#include <signal.h>
#include <unistd.h>
#include <termios.h>

#define debug_print printf

double get_time()
{
  static struct timeval temp;
  gettimeofday(&temp,NULL);
  return temp.tv_sec + temp.tv_usec*0.000001;
}


// Constructor
SerialPort::SerialPort()
{
  // Initialize all the variables
  this->fd_                = -1;
  this->connected_         = 0;
  this->baud_              = B2400;
}

// Destructor
SerialPort::~SerialPort()
{
  disconnect();
}


int SerialPort::connect(const char * device, const char * desired_baud_rate_str)
{
  int baud_rate = strtol(desired_baud_rate_str,NULL,10);
  return connect(device,baud_rate);
}

//connect to the serial device
int SerialPort::connect(const char * device, const int baud_rate)
{

  if (connected_)
  {
    debug_print("already connected\n");
    return 0;
  }

  //store the device name
  strncpy(device_,device,MAX_DEVICE_NAME_LENGTH);

  // Open the device
  //if((fd_ = open(device_, O_RDWR | O_NOCTTY | O_NONBLOCK)) < 0)
  if((fd_ = open(device_, O_RDWR | O_NOCTTY)) < 0)
  {
    debug_print("Error: Unable to open serial port\n");
    return -1;
  }

  fstat(fd_,&fd_stat_);         //get the file descriptor information
  
  int flags;
  if((flags = fcntl(fd_,F_GETFL)) < 0)             // get the current flags
  {
    debug_print("unable to get device flags\n");
    close(fd_);
    return -1;
  } 

  
  if(fcntl(fd_,F_SETFL,flags & (~O_NONBLOCK)) < 0)  // set the new flags
  {
    debug_print("unable to set device flags\n");
    close(fd_);
    return -1;
  } 
    
   
  //save current attributes so they can be restored after use
  if( tcgetattr( fd_, &old_term_ ) < 0 )
  {
    debug_print("unable to get old serial port attributes\n");
    close(fd_);
    return -1;
  }
  
  connected_=1;
  
  //set up the terminal and set the baud rate
  if (set_baud_rate(baud_rate))
  {
    debug_print("unable to set baud rate\n");
    close(fd_);
    return -1;
  }
  
  return 0;
}

//disconnect from the device
int SerialPort::disconnect()
{
  //check whether we are connected to the device  
  if (!connected_)
    return 0;

  if(tcsetattr(fd_,TCSANOW,&old_term_) < 0)  // Restore old terminal settings
    debug_print("failed to restore attributes!\n");
  
  // Actually close the device
  if(close(fd_) != 0)
  {
    debug_print("failed to close device!\n");
    connected_=0;
    return -1;
  }
  
  connected_=0;
  
  return 0;
}



int SerialPort::speed_to_baud_rate(int speed, speed_t & baud)
{
  switch (speed) 
  {
    case 2400:    baud=B2400;    return 0;
    case 4800:    baud=B4800;    return 0;
    case 9600:    baud=B9600;    return 0;
    case 19200:   baud=B19200;   return 0;
    case 38400:   baud=B38400;   return 0;
    case 57600:   baud=B57600;   return 0;
    case 115200:  baud=B115200;  return 0;
    case 230400:  baud=B230400;  return 0;
#ifndef __APPLE__
    case 460800:  baud=B460800;  return 0;
    case 921600:  baud=B921600;  return 0;
    case 1000000: baud=B1000000; return 0;
    case 2000000: baud=B2000000; return 0;
#endif
  }
  
  //debug_print("unknown baud rate\n");
  return -1;
}

int SerialPort::set_standard_baud_rate(speed_t baud)
{
#ifndef __APPLE__
  //Get serial port info
  if (ioctl(fd_, TIOCGSERIAL, &(serial_)) < 0)
  {
    debug_print("ioctl() failed while trying to get serial port info");
  }
  //make sure that custom speed option is turned off
  else
  {
    serial_.flags         &= ~ASYNC_SPD_CUST;
    serial_.flags         |= ASYNC_LOW_LATENCY;  //will set ftdi delay to 1ms
    serial_.custom_divisor = 0;
    if (ioctl(fd_, TIOCSSERIAL, &(serial_)) < 0) 
      debug_print("ioctl() failed while trying to set serial port info");
  } 
#endif
  
  if( tcgetattr(fd_, &new_term_) < 0 )
  {
    debug_print("Unable to get serial port attributes\n");
    return -1;
  }

  cfmakeraw( &new_term_ );  //cfmakeraw initializes the port to standard configuration
  
  if (cfsetispeed(&new_term_, baud) < 0 )   //set input baud rate
  {
    debug_print("Unable to set baud rate\n");
    return -1;
  }
  
 
  if (cfsetospeed(&new_term_, baud) < 0 )    //set output baud rate
  {
    debug_print("Unable to set baud rate\n");
    return -1;
  }
  
  //set new attributes 
  if( tcsetattr(fd_, TCSAFLUSH, &new_term_) < 0 )
  {
    debug_print("Unable to set serial port attributes\n");
    return -1;
  }
  
  tcflush(fd_, TCIOFLUSH);
  baud_=baud;
  
  return 0;
}


int SerialPort::set_baud_rate(const int speed)
{
  speed_t temp_baud_rate;

  //check whether we are connected to the device
  if (!connected_)
  {
    debug_print("not connected to the device\n");
    return -1;
  }

  //convert the integer speed value to speed_t if needed
  int non_standard_baud_rate = speed_to_baud_rate(speed,temp_baud_rate);
  if (non_standard_baud_rate == 0)
  {
    if (set_standard_baud_rate(temp_baud_rate) != 0)
    {
      debug_print("could not set standard baud rate\n");
      return -1;
    }
  }
  else
  {
    if (set_non_standard_baud_rate(speed) != 0)
    {
      debug_print("could not set non-standard baud rate\n");
      return -1;
    }
  }

  return 0;
}

#ifndef __APPLE__
int SerialPort::set_non_standard_baud_rate(int speed)
{
  if (speed < 300)
  {
    debug_print("bad desired speed\n");
    return -1;
  }
  
  //debug_print("setting nonstandard baud rate %d\n",speed);
  
  if (ioctl(fd_, TIOCGSERIAL, &(serial_)) < 0)
  {
    debug_print("ioctl() failed while trying to get serial port info\n");
  }
  //make sure that custom speed option is turned off
  else
  {
    serial_.flags         |= ASYNC_SPD_CUST | ASYNC_LOW_LATENCY;
    serial_.custom_divisor = 24000000/speed;
    if (ioctl(fd_, TIOCSSERIAL, &(serial_)) < 0) 
      debug_print("ioctl() failed while trying to set serial port info\n");
  }
    
  //get current port settings
  if( tcgetattr(fd_, &new_term_) < 0 )
  {
    debug_print("Unable to get serial port attributes\n");
    return -1;
  }

  //cfmakeraw initializes the port to standard configuration. Use this!
  cfmakeraw( &new_term_ );
  
  //set input baud rate (B38400 is for custom)
  if (cfsetispeed( &new_term_, B38400 ) < 0 )
  {
    debug_print("Unable to set baud rate\n");
    return -1;
  }
  
  //set output baud rate (B38400 is for custom)
  if (cfsetospeed( &new_term_, B38400 ) < 0 )
  {
    debug_print("Unable to set baud rate\n");
    return -1;
  }
   
  //set new attributes 
  if( tcsetattr(fd_, TCSAFLUSH, &new_term_) < 0 )
  {
    debug_print("Unable to set serial port attributes\n");
    return -1;
  }
  
  tcflush(fd_, TCIOFLUSH);
  baud_=speed;
  
  return 0;
}

#else
int SerialPort::set_non_standard_baud_rate(int speed)
{
  if (speed < 300)
  {
    debug_print("bad desired speed\n");
    return -1;
  }
  
  debug_print("setting nonstandard baud rate %d\n",speed);
  
  //get current port settings
  if( tcgetattr(fd_, &new_term_ ) < 0 )
  {
    debug_print("Unable to get serial port attributes\n");
    return -1;
  }

  //cfmakeraw initializes the port to standard configuration. Use this!
  cfmakeraw( &new_term_ );
  
  //set input baud rate (does not matter since IOSSIOSPEED will override)
  if (cfsetispeed( &new_term_, B38400 ) < 0 )
  {
    debug_print("Unable to set baud rate\n");
    return -1;
  }
  
  //set input baud rate (does not matter since IOSSIOSPEED will override)
  if (cfsetospeed(&new_term_, B38400) < 0 )
  {
    debug_print("Unable to set baud rate\n");
    return -1;
  }
    
  //set new attributes 
  if( tcsetattr(fd_, TCSAFLUSH, &new_term_) < 0 )
  {
    debug_print("Unable to set serial port attributes\n");
    return -1;
  }
  
  tcflush(fd_, TCIOFLUSH);
  
  //set the custom speed using ioctl (osx-specifict)
  speed_t spd = speed;
  if (ioctl(fd_, IOSSIOSPEED, &spd) < 0)
  {
    debug_print("could not set custom speed\n");
    return -1;
  }
  
  //set the custom latency using ioctl (osx-specifict)
  unsigned long latency_us = 1000;
    
  if (ioctl(this->fd_, IOSSDATALAT, &latency_us) < 0)
  {
    debug_print("could not set latency\n");
    return -1;
  }
  
  tcflush(fd_, TCIOFLUSH);
  baud_=speed;
    
  return 0;
}
#endif


//read characters from device
int SerialPort::read_data(char * data, int byte_count, int timeout_us)
{
  
  double t_entry  = get_time();
  double t_wait   = timeout_us * 0.000001;
  double t_finish = t_entry + t_wait;
  
  //check whether we are connected to the device
  if (!connected_)
  {
    debug_print("not connected to the device\n");
    return -1;
  }

  fd_set watched_fds;
  struct timeval timeout;
  int bytes_read_total = 0;
  int bytes_left = byte_count;
  int retval;
  int bytes_read;

  
  struct stat fd_stat;
  fstat(fd_,&fd_stat);
  
  if (fd_stat.st_mode != fd_stat_.st_mode)
  {
    debug_print("device disconnected???");
    usleep(timeout_us);  //sleep to avoid high cpu usage
    return -10;
  }
        
  while (bytes_left > 0) 
  {
    //set up for the "select" call
    FD_ZERO(&watched_fds);
    FD_SET(fd_, &watched_fds);
    double tnow = get_time();
    double dt = t_finish - tnow;

    if (dt < 0)
      return bytes_read_total;

    timeout.tv_sec = (int)dt;
    timeout.tv_usec = (dt-timeout.tv_sec)*1000000.0;
  
    if ((retval = select(fd_ + 1, &watched_fds, NULL, NULL, &timeout)) < 1)   //block until at least 1 char is available or timeout
    {                                                                         //error reading chars
      if (retval < 0)
      {
        debug_print("select call failed\n");
      }
      else                                                                    //timeout
      {
      }
    }
    else
    {
      bytes_read        = read(fd_, &(data[bytes_read_total]), bytes_left);
    
      if (bytes_read > 0)
      {
        bytes_read_total += bytes_read;
        bytes_left       -= bytes_read;
      }
    }
  }
  return bytes_read_total;
}

int SerialPort::write_data(const char * data, int byte_count, int delay_us)
{
  int bytes_written_total=0;
  int bytes_written;
  int bytes_left=byte_count;
    
  
  //check whether we are connected to the device  
  if (!connected_)
  {
    debug_print("not connected to the device\n");
    return -1;
  }
  
  if (delay_us==0)
    bytes_written_total=write(fd_,data,byte_count);
  
  else
  {
    while (bytes_left)
    {
      bytes_written=write(fd_,&(data[bytes_written_total]),1);
      if (bytes_written < 0)
      {
        debug_print("error writing\n");
        return -1;
      }
      if (bytes_written < 1)
      {
        debug_print("could not write a char\n");
        return bytes_written_total;
      }

      bytes_written_total += bytes_written;
      bytes_left          -= bytes_written;
      usleep(delay_us);
    }
  }  
  
  //tcdrain(this->fd);   //wait till all the data written to the file descriptor is transmitted

  return bytes_written_total;
}
