/****************************************************************************
 * Copyright (c) 2017 The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name The Linux Foundation nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * NO EXPRESS OR IMPLIED LICENSES TO ANY PARTY'S PATENT RIGHTS ARE GRANTED BY 
 * THIS LICENSE.
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * In addition Supplemental Terms apply.  See the SUPPLEMENTAL file.
 *
 ****************************************************************************/

/*
 * This file provides the class definition of a serial port wrapper for communicating with UART devices
 * using standard Linux file I/O
 */

#ifndef SERIAL_PORT_HPP
#define SERIAL_PORT_HPP

#include <iostream>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <termios.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <string.h>

#ifdef __APPLE__
#include <IOKit/serial/ioss.h>
#else
#include <linux/serial.h>
#endif

#define MAX_DEVICE_NAME_LENGTH 128
#define DEFAULT_READ_TIMEOUT_US 1000000

/**
 * Wrapper for Linux serial port communication using standard file I/O
 */
class SerialPort
{
  public:

    /**
     * Constructor
     */
    SerialPort();
    
    /**
     * Destructor
     */
    ~SerialPort();

    /**
     * Connect to serial port.
     * @param device
     *   Pointer to the device path (null-terminated character string)
     * @param desired_baud_rate_str
     *   Pointer to the desired baud rate represented as null-terminated character string
     * @return int
     *   0 = success
     *   otherwise = failure
     */
    int connect(const char * device, const char * desired_baud_rate_str);
    
    /**
     * Connect to serial port.
     * @param device
     *   Pointer to the device path (null-terminated character string)
     * @param desired_baud_rate
     *   Desired baud rate represented as an integer 
     * @return int
     *   0 = success
     *   otherwise = failure
     */
    int connect(const char * device, const int desired_baud_rate);
    
    /**
     * Connect to serial port.
     *   @return int
     *   0 = success
     *   otherwise = failure
     */
    int disconnect();
    
    /**
     * Set desired baud rate after connection has alreade been made with connect() call
     * @param device
     *   Pointer to the device path (null-terminated character string)
     * @param baud_rate
     *   Desired baud rate represented as an integer 
     * @return int
     *   0 = success
     *   otherwise = failure
     */
    int set_baud_rate(const int baud_rate);
    
    
    /**
     * Check if device is connected (file descriptor is valid) 
     * @return int
     *   1 = success
     *   otherwise = failure
     */
    inline int is_connected()
    {
      return connected_;
    }
    
    /**
     * Read data from the serial port (has to be already open)
     * @param data
     *   Pointer to the destination buffer
     * @param byte_count
     *   Maximum number of bytes to read
     * @param timeout_us
     *   Timeout in microseconds = maximum blockin time before the function must return regardless of number of bytes read
     * @return int
     *   number of bytes read (if non-negative)
     *   otherwise = failure
     */
    int read_data(char * data, int byte_count, int timeout_us=DEFAULT_READ_TIMEOUT_US);
    
    /**
     * Write data to the serial port (has to be already open)
     * @param data
     *   Pointer to the source buffer
     * @param byte_count
     *   Number of bytes to write
     * @param delay_us
     *   Delay in microseconds = if non-zero, sleep for this long between writing each character
     * @return int
     *   number of bytes written (if non-negative)
     *   otherwise = failure
     */
    int write_data(const char * data, int byte_count, int delay_us=0);                       //write a number of characters

  private:

    char     device_[MAX_DEVICE_NAME_LENGTH];               //devince name
    speed_t  baud_;                                         //baud rate
    int      fd_;                                           //file descriptor
    int      connected_;                                    //status

    int      set_io_block_with_timeout();
    int      set_blocking_io();                             //set blocking IO
    int      set_non_blocking_io();                         //set non-blocking IO
    int      speed_to_baud_rate(int speed, speed_t & baud); //convert integer speed to baud rate setting

    int set_standard_baud_rate(speed_t baud);
    int set_non_standard_baud_rate(int baud);
    

    struct termios old_term_, new_term_;                    //terminal structs
    struct stat fd_stat_;                                   //mode of the file descriptor at the time of opening for checking if still connected
    
    #ifndef __APPLE__
    struct serial_struct serial_;                           //struct for setting custom baud rates on OSX systems
    #endif
};


#endif //SERIAL_PORT_HPP
